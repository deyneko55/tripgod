package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/account/dashboards")
@CrossOrigin(":4200")
public class DashboardController {
    //To implement:
    //1. locations, carriers increasing
    //2. costs(per day, per week, per month, per carrier)
    //3. trouble tickets statistics
    @Autowired
    private DashboardService dashboardService;

    //@Secured(RoleName.ROLE_ADMIN)
    @GetMapping()
    public ResponseEntity<?> loadDashboardInfo() {

        return ResponseEntity.ok(dashboardService.getDashboardResponse());
    }
}
