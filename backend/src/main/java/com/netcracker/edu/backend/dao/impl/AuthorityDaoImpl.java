package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.AuthorityDao;
import com.netcracker.edu.backend.mapper.AuthorityMapper;
import com.netcracker.edu.backend.model.Authority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class AuthorityDaoImpl extends GenericDaoImpl<Authority> implements AuthorityDao {

    @Value("${db.query.authority.insert}")
    private String sqlAuthorityInsert;

    @Value("${db.query.authority.update}")
    private String sqlAuthorityUpdate;

    @Value("${db.query.authority.getByName}")
    private String sqlGetAuthorityByName;

    public AuthorityDaoImpl() {
        super(new AuthorityMapper(), TableName.AUTHORITY);
    }

    @Override
    protected String getInsertSql() {
        return sqlAuthorityInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Authority entity) throws SQLException {

        int argNum = 1;
        statement.setString(argNum++, entity.getName());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlAuthorityUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Authority entity) {
        return new Object[]{entity.getName()};
    }

    @Override
    public Optional<Authority> getAuthorityByName(String name) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetAuthorityByName, new Object[]{name}, rowMapper));
    }
}
