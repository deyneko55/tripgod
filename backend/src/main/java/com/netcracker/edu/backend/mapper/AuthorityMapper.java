package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Authority;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.AuthorityColumns;

public class AuthorityMapper implements RowMapper<Authority> {

    @Override
    public Authority mapRow(ResultSet resultSet, int i) throws SQLException {
        Authority authority = new Authority();

        authority.setId(resultSet.getLong(AuthorityColumns.AUTHORITY_ID));
        authority.setName(resultSet.getString(AuthorityColumns.NAME));

        return authority;
    }

}
