package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.ServiceType;

@RestController
@RequestMapping("/api/account/services")
@CrossOrigin(":4200")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @GetMapping()
    public ResponseEntity<?> loadAllServices() {
        return ResponseEntity.ok(serviceService.getAllServices());
    }

    @PostMapping
    public ResponseEntity<?> createService(@Valid @RequestBody com.netcracker.edu.backend.model.Service service) {
        service.setType(ServiceType.SERVICE); // dummy
        return ResponseEntity.ok(serviceService.createService(service));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateService(@Valid @RequestBody com.netcracker.edu.backend.model.Service service) {
        serviceService.updateService(service);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteService(@PathVariable long id) {
        serviceService.deleteServiceById(id);
        return ResponseEntity.ok().build();
    }
    //To impl: specific methods etc
}
