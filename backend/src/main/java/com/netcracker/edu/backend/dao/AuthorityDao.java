package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Authority;

import java.util.Optional;

public interface AuthorityDao extends GenericDao<Authority> {

    Optional<Authority> getAuthorityByName(String name);

}
