package com.netcracker.edu.backend.mapper;


import com.netcracker.edu.backend.model.Discount;
import com.netcracker.edu.backend.model.Service;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.*;

public class DiscountDetailedMapper implements RowMapper<Discount> {

    @Override
    public Discount mapRow(ResultSet resultSet, int i) throws SQLException {
        Discount discount = new Discount();

        discount.setId(resultSet.getLong(DiscountColumns.DISCOUNT_ID));
        discount.setServiceId(resultSet.getLong(DiscountColumns.SERVICE_ID));
        discount.setTypeId(resultSet.getLong(DiscountColumns.TYPE_ID));
        discount.setAmount(resultSet.getDouble(DiscountColumns.AMOUNT));
        discount.setStartDate(resultSet.getTimestamp(DiscountColumns.START_DATE));
        discount.setEndDate(resultSet.getTimestamp(DiscountColumns.END_DATE));

        discount.setType(resultSet.getString(DiscountTypeColumns.TYPE_NAME));

        Service service = new Service();

        service.setId(resultSet.getLong(ServiceColumns.SERVICE_ID));
        service.setTypeId(resultSet.getLong(ServiceColumns.TYPE_ID));
        service.setName(resultSet.getString(ServiceColumns.NAME));
        service.setApproverId(resultSet.getLong(ServiceColumns.APPROVER_ID));
        service.setProviderId(resultSet.getLong(ServiceColumns.PROVIDER_ID));
        service.setStatusId(resultSet.getLong(ServiceColumns.STATUS_ID));
        service.setLocationId(resultSet.getLong(ServiceColumns.LOCATION_ID));
        service.setDestinationId(resultSet.getLong(ServiceColumns.DESTINATION_ID));
        service.setNumberOfPeople(resultSet.getLong(ServiceColumns.NUMBER_OF_PEOPLE));
        service.setPrice(resultSet.getBigDecimal(ServiceColumns.PRICE));
        service.setDescription(resultSet.getString(ServiceColumns.DESCRIPTION));
        service.setImgSrc(resultSet.getString(ServiceColumns.IMAGE_SRC));

        discount.setService(service);

        return discount;
    }
}