package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.UserDao;
import com.netcracker.edu.backend.mapper.UserDetailedMapper;
import com.netcracker.edu.backend.mapper.UserMapper;
import com.netcracker.edu.backend.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    @Value("${db.query.user.insert}")
    private String sqlUserInsert;

    @Value("${db.query.user.update}")
    private String sqlUserUpdate;

    @Value("${db.query.user.getByUsername}")
    private String sqlGetUserByUsername;

    @Value("${db.query.user.getByEmail}")
    private String sqlGetUserByEmail;

    @Value("${db.query.user.getByUsernameOrEmail}")
    private String sqlGetUserByUsernameOrEmail;

    @Value("${db.query.user.getDetailedById}")
    private String sqlGetDetailedUserById;

    @Value("${db.query.user.getDetailedByUsername}")
    private String sqlGetDetailedUserByUsername;

    @Value("${db.query.user.getDetailedByEmail}")
    private String sqlGetDetailedUserByEmail;

    @Value("${db.query.user.getDetailedByUsernameOrEmail}")
    private String sqlGetDetailedUserByUsernameOrEmail;

    private static final RowMapper<User> userRowMapperDetailed = new UserDetailedMapper();

    public UserDaoImpl() {
        super(new UserMapper(), TableName.USER);
    }

    @Override
    protected String getInsertSql() {
        return sqlUserInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, User entity) throws SQLException {

        int argNum = 1;
        statement.setLong(argNum++, entity.getAuthorityId());
        statement.setString(argNum++, entity.getEmail());
        statement.setString(argNum++, entity.getUsername());
        statement.setString(argNum++, entity.getPassword());
        statement.setBoolean(argNum++, entity.isActive());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlUserUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(User entity) {
        return new Object[]{entity.getAuthorityId(), entity.getEmail(), entity.getUsername(), entity.getPassword(), entity.isActive()};
    }

    @Override
    public Optional<User> getUserByUsernameOrEmail(String usernameOrEmail) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetUserByUsernameOrEmail, new Object[]{usernameOrEmail, usernameOrEmail}, rowMapper));
    }

    @Override
    public boolean existsByUsername(String username) {
        return !(jdbcTemplate.query(sqlGetUserByUsername, new Object[]{username}, rowMapper)).isEmpty();
    }

    @Override
    public boolean existsByEmail(String email) {
        return !(jdbcTemplate.query(sqlGetUserByEmail, new Object[]{email}, rowMapper)).isEmpty();
    }

    @Override
    public Optional<User> getDetailedUserById(long id) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetDetailedUserById, new Object[]{id}, userRowMapperDetailed));
    }

    @Override
    public Optional<User> getDetailedUserByUsername(String username) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetDetailedUserByUsername, new Object[]{username}, userRowMapperDetailed));
    }

    @Override
    public Optional<User> getDetailedUserByEmail(String email) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetDetailedUserByEmail, new Object[]{email}, userRowMapperDetailed));
    }

    @Override
    public Optional<User> getDetailedUserByUsernameOrEmail(String usernameOrEmail) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetDetailedUserByUsernameOrEmail, new Object[]{usernameOrEmail, usernameOrEmail}, userRowMapperDetailed));
    }
}
