package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.AuthorityDao;
import com.netcracker.edu.backend.dao.UserDao;
import com.netcracker.edu.backend.dao.UserDetailsDao;
import com.netcracker.edu.backend.dto.request.LoginRequest;
import com.netcracker.edu.backend.dto.request.SignUpRequest;
import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.Authority;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.model.UserDetails;
import com.netcracker.edu.backend.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;

import static com.netcracker.edu.backend.utils.Constants.ResponseEntities;


@Service
@Transactional
public class AuthService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserDetailsDao userDetailsDao;

    @Autowired
    private AuthorityDao authorityDao;

    @Transactional
    public ResponseEntity registerUser(SignUpRequest request, PasswordEncoder encoder) {

        if (userDao.existsByUsername(request.getUsername())) {
            return ResponseEntities.BAD_REQ_USERNAME_TAKEN;
        }
        if (userDao.existsByEmail(request.getEmail())) {
            return ResponseEntities.BAD_REQ_EMAIL_TAKEN;
        }

        Authority userAuthority = authorityDao.getAuthorityByName(request.getRole())
                .orElseThrow(() -> new AppException("User Role not set."));


        User user = new User(userAuthority.getId(), request.getEmail(), request.getUsername(),
                encoder.encode(request.getPassword()), false);

        userDao.insert(user);

        UserDetails userDetails = new UserDetails(user.getId(), request.getFirstName(), request.getLastName(),
                Timestamp.from(Instant.now()));

        userDetailsDao.insert(userDetails);

        return ResponseEntities.USER_REGISTERED_SUCCESSFULLY;
    }

    public String authenticateUser(LoginRequest request, AuthenticationManager authManager, JwtTokenProvider tokenProvider) {

        Authentication authentication = authManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsernameOrEmail(),
                        request.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return tokenProvider.generateToken(authentication);
    }
}
