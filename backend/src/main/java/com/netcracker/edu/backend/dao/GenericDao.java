package com.netcracker.edu.backend.dao;

import java.util.List;
import java.util.Optional;

public interface GenericDao<T> {

    List<T> getAll();

    List<T> getAllWithSimpleCheck(String columnName, Object columnValue);

    Optional<T> getById(Long id);

    T insert(T entity);

    void update(T entity);

    void delete(T entity);

    void deleteById(Long id);

}
