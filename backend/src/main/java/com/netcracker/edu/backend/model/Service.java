package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class Service extends Identified {

    private long typeId;

    private String name;

    private Long approverId;

    private long providerId;

    private long statusId;

    private long locationId;

    private Long destinationId;

    private long numberOfPeople;

    private BigDecimal price;

    private String description;

    private String imgSrc;


    private String type;

    private String status;

    private User approver;

    private User provider;

    private String location;

    private String destination;

    private List<Service> bundleTrips;

    private List<Service> tripServices;

    public Service(long typeId, String name, long providerId, long statusId, long locationId, long numberOfPeople, BigDecimal price, String description) {
        this.typeId = typeId;
        this.name = name;
        this.providerId = providerId;
        this.statusId = statusId;
        this.locationId = locationId;
        this.numberOfPeople = numberOfPeople;
        this.price = price;
        this.description = description;
    }

    public Service(long typeId, String name, Long approverId, long providerId, long statusId, long locationId, Long destinationId, long numberOfPeople, BigDecimal price, String description, String imgSrc) {
        this.typeId = typeId;
        this.name = name;
        this.approverId = approverId;
        this.providerId = providerId;
        this.statusId = statusId;
        this.locationId = locationId;
        this.destinationId = destinationId;
        this.numberOfPeople = numberOfPeople;
        this.price = price;
        this.description = description;
        this.imgSrc = imgSrc;
    }
}
