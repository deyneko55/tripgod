package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.UserDetailsDao;
import com.netcracker.edu.backend.mapper.UserDetailsMapper;
import com.netcracker.edu.backend.model.UserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
@Slf4j
public class UserDetailsDaoImpl extends GenericDaoImpl<UserDetails> implements UserDetailsDao {

    @Value("${db.query.user_details.insert}")
    private String sqlUserDetailsInsert;

    @Value("${db.query.user_details.update}")
    private String sqlUserDetailsUpdate;

    public UserDetailsDaoImpl() {
        super(new UserDetailsMapper(), TableName.USER_DETAILS);
    }

    @Override
    protected String getInsertSql() {
        return sqlUserDetailsInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, UserDetails entity) throws SQLException {

        int argNum = 1;

        statement.setLong(argNum++, entity.getId());
        statement.setString(argNum++, entity.getFirstName());
        statement.setString(argNum++, entity.getLastName());
        statement.setTimestamp(argNum++, entity.getRegistrationDate());

        Long locationId = entity.getLocationId();
        if (locationId == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setLong(argNum++, locationId);
        }

        statement.setString(argNum++, entity.getImageSrc());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlUserDetailsUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(UserDetails entity) {
        return new Object[]{entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getRegistrationDate(),
                entity.getLocationId(), entity.getImageSrc()};
    }
}
