package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.UserColumns;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();

        user.setId(resultSet.getLong(UserColumns.USER_ID));
        user.setAuthorityId(resultSet.getLong(UserColumns.AUTHORITY_ID));
        user.setUsername(resultSet.getString(UserColumns.USERNAME));
        user.setEmail(resultSet.getString(UserColumns.EMAIL));
        user.setPassword(resultSet.getString(UserColumns.PASSWORD));
        user.setActive(resultSet.getBoolean(UserColumns.IS_ACTIVE));
        return user;
    }
}
