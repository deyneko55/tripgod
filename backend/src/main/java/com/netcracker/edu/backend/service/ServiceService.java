package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceService {

    @Autowired
    private ServiceDao serviceDao;

    public Optional<com.netcracker.edu.backend.model.Service> getServiceById(long id) {
        return serviceDao.getServiceById(id);
    }

    public com.netcracker.edu.backend.model.Service createService(com.netcracker.edu.backend.model.Service service) {
        return serviceDao.insert(service);
    }

    public void updateService(com.netcracker.edu.backend.model.Service service) {
        serviceDao.update(service);
    }

    public void deleteServiceById(long id) {
        serviceDao.deleteById(id);
    }

    public void deleteService(com.netcracker.edu.backend.model.Service service) {
        serviceDao.delete(service);
    }

    public List<com.netcracker.edu.backend.model.Service> getAllServices() {
        return serviceDao.getAllServices();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllServicesOfStatus(String status) {
        return serviceDao.getAllServicesOfStatus(status);
    }

    public List<com.netcracker.edu.backend.model.Service> getServicesByProviderId(long id) {
        return serviceDao.getServicesByProviderId(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getServicesOfStatusByProviderId(String status, long id) {
        return serviceDao.getServicesOfStatusByProviderId(status, id);
    }

    public long countAllServices() {
        return serviceDao.countAllServices();
    }
}
