package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BundleService {

    @Autowired
    private ServiceDao serviceDao;

    public Optional<com.netcracker.edu.backend.model.Service> getBundleById(long id) {
        return serviceDao.getServiceById(id);
    }

    public com.netcracker.edu.backend.model.Service createBundle(com.netcracker.edu.backend.model.Service service) {
        return serviceDao.insert(service);
    }

    public void updateBundle(com.netcracker.edu.backend.model.Service service) {
        serviceDao.update(service);
    }

    public void deleteBundleById(long id) {
        serviceDao.deleteById(id);
    }

    public void deleteBundle(com.netcracker.edu.backend.model.Service service) {
        serviceDao.delete(service);
    }

    public List<com.netcracker.edu.backend.model.Service> getAllBundles() {
        return serviceDao.getAllBundles();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllBundlesOfStatus(String status) {
        return serviceDao.getAllBundlesOfStatus(status);
    }

    public List<com.netcracker.edu.backend.model.Service> getBundlesByProviderId(long id) {
        return serviceDao.getBundlesByProviderId(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getBundlesOfStatusByProviderId(String status, long id) {
        return serviceDao.getBundlesOfStatusByProviderId(status, id);
    }

    public long countAllBundles() {
        return serviceDao.countAllBundles();
    }
}
