package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
public class UserDetails extends Identified {

    private String firstName;

    private String lastName;

    private Timestamp registrationDate;

    private Long locationId;

    private String imageSrc;

    public UserDetails(long id, String firstName, String lastName, Timestamp registrationDate) {

        setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.registrationDate = registrationDate;
    }

    public UserDetails(long id, String firstName, String lastName, Timestamp registrationDate, long locationId, String imageSrc) {

        setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.registrationDate = registrationDate;
        this.locationId = locationId;
        this.imageSrc = imageSrc;
    }
}
