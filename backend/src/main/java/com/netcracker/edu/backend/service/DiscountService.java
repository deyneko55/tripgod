package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.DiscountDao;
import com.netcracker.edu.backend.model.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class DiscountService {

    @Autowired
    private DiscountDao discountDao;

    public Optional<Discount> getDiscountById(long id) {
        return discountDao.getById(id);
    }

    public Discount createDiscount(Discount discount) {
        return discountDao.insert(discount);
    }

    public void updateDiscount(Discount discount) {
        discountDao.update(discount);
    }

    public void deleteDiscountById(long id) {
        discountDao.deleteById(id);
    }

    public void deleteDiscount(Discount discount) {
        discountDao.delete(discount);
    }

    public List<Discount> getAllDiscounts() {
        return discountDao.getAll();
    }

    public List<Discount> getDiscountsOfType(String type) {
        return discountDao.getDiscountsOfType(type);
    }

    public List<Discount> getDiscountsByServiceId(long serviceId) {
        return discountDao.getDiscountsByServiceId(serviceId);
    }

    public List<Discount> getDiscountsForPeriod(Timestamp from, Timestamp to) {
        return discountDao.getDiscountsForPeriod(from, to);
    }

    public List<Discount> getDiscountsOfTypeDetailed(String type) {
        return discountDao.getDiscountsOfTypeDetailed(type);
    }

    public List<Discount> getDiscountsByServiceIdDetailed(long serviceId) {
        return discountDao.getDiscountsByServiceIdDetailed(serviceId);
    }

    public List<Discount> getDiscountsForPeriodDetailed(Timestamp from, Timestamp to) {
        return discountDao.getDiscountsForPeriodDetailed(from, to);
    }
}
