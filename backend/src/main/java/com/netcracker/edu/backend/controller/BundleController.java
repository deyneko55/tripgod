package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Service;
import com.netcracker.edu.backend.service.BundleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.ServiceType;

@RestController
@RequestMapping("/api/account/bundles")
@CrossOrigin(":4200")
public class BundleController {

    @Autowired
    private BundleService bundleService;

    @GetMapping()
    public ResponseEntity<?> loadAllBundles() {
        return ResponseEntity.ok(bundleService.getAllBundles());
    }

    @PostMapping("/create")
    public ResponseEntity<?> createBundle(@Valid @RequestBody Service service) {
        service.setType(ServiceType.BUNDLE); // dummy
        return ResponseEntity.ok(bundleService.createBundle(service));
    }

    @PutMapping("/bundle/update/{id}")
    public ResponseEntity<?> updateBundle(@Valid @RequestBody Service service) {
        bundleService.updateBundle(service);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/bundle/delete/{id}")
    public ResponseEntity<?> deleteBundle(@PathVariable long id) {
        bundleService.deleteBundleById(id);
        return ResponseEntity.ok().build();
    }
    //To impl: specific methods etc
}
