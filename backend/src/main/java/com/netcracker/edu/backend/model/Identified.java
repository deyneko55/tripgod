package com.netcracker.edu.backend.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Identified {

    protected Long id;

}
