package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.UserDetails;

public interface UserDetailsDao extends GenericDao<UserDetails> {
}
