package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.service.UserService;
import com.netcracker.edu.backend.utils.Constants.ResponseEntities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/account/users")
@CrossOrigin(":4200")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping()
    public ResponseEntity<?> loadAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    //?????????????????
    @PostMapping("/create/approver")
    public ResponseEntity<?> createApprover(@Valid @RequestBody User user) {
        userService.createUser(user);
        return ResponseEntities.APPROVER_REGISTERED_SUCCESSFULLY;
    }

    @PostMapping("/create/provider")
    public ResponseEntity<?> createProvider(@Valid @RequestBody User user) {
        userService.createUser(user);
        return ResponseEntities.PROVIDER_REGISTERED_SUCCESSFULLY;
    }
    //????????????????

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable long id) {
        userService.deleteUserById(id);
        return ResponseEntity.ok().build();
    }
    //To impl: specific methods etc
}
