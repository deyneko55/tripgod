package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.Discount;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.DiscountColumns;

public class DiscountMapper implements RowMapper<Discount> {

    @Override
    public Discount mapRow(ResultSet resultSet, int i) throws SQLException {
        Discount discount = new Discount();

        discount.setId(resultSet.getLong(DiscountColumns.DISCOUNT_ID));
        discount.setServiceId(resultSet.getLong(DiscountColumns.SERVICE_ID));
        discount.setTypeId(resultSet.getLong(DiscountColumns.TYPE_ID));
        discount.setAmount(resultSet.getDouble(DiscountColumns.AMOUNT));
        discount.setStartDate(resultSet.getTimestamp(DiscountColumns.START_DATE));
        discount.setEndDate(resultSet.getTimestamp(DiscountColumns.END_DATE));

        return discount;
    }
}