package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.AuthorityDao;
import com.netcracker.edu.backend.dao.UserDao;
import com.netcracker.edu.backend.dao.UserDetailsDao;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserDetailsDao userDetailsDao;

    @Autowired
    private AuthorityDao authorityDao;

    public Optional<User> getUserById(Long id) {
        return userDao.getById(id);
    }

    public Optional<User> getDetailedUserById(Long id) {
        return userDao.getDetailedUserById(id);
    }

    public boolean existsByUsername(String username) {
        return userDao.existsByUsername(username);
    }

    public boolean existsByEmail(String email) {
        return userDao.existsByEmail(email);
    }

    public Optional<User> getUserByUsernameOrEmail(String usernameOrEmail) {
        return userDao.getUserByUsernameOrEmail(usernameOrEmail);
    }

    public List<User> getAllUsers() {
        return userDao.getAll();
    }

    public void deleteUserById(long id) {
        userDetailsDao.deleteById(userDao.getDetailedUserById(id).get().getDetails().getId());
        userDao.deleteById(id);
    }

    public void deleteUser(User user) {
        userDetailsDao.delete(user.getDetails());
        userDao.delete(user);
    }

    public void updateUser(User user) {
        userDao.update(user);
    }

    public User createUser(User user) {
        return userDao.insert(user);
    }

    public UserDetails addUserDetails(UserDetails userDetails) {
        return userDetailsDao.insert(userDetails);
    }

    public void updateUserDetails(UserDetails userDetails) {
        userDetailsDao.update(userDetails);
    }
}
