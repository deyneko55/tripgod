package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

// Future User replacement
@Data
@NoArgsConstructor
public class User extends Identified {

    private long authorityId;

    private String email;

    private String username;

    private String password;

    private boolean isActive;


    private String authority;

    private UserDetails details;

    public User(long authorityId, String email, String username, String password, boolean isActive) {
        this.authorityId = authorityId;
        this.email = email;
        this.username = username;
        this.password = password;
        this.isActive = isActive;
    }
}
