package com.netcracker.edu.backend.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
public class Discount extends Identified {

    private long serviceId;

    private long typeId;

    private double amount;

    private Timestamp startDate;

    private Timestamp endDate;


    private Service service;

    private String type;

    public Discount(long serviceId, long typeId, double amount, Timestamp startDate, Timestamp endDate) {
        this.serviceId = serviceId;
        this.typeId = typeId;
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
