package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.exception.AppException;
import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.security.CurrentUser;
import com.netcracker.edu.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/account/summary")
@CrossOrigin(":4200")
public class ProfileController {

    @Autowired
    private UserService userService;

    @GetMapping()
    public ResponseEntity<?> loadUserAndDetailsForSummary(@CurrentUser User currUser) {
        User user = userService.getDetailedUserById(currUser.getId())
                .orElseThrow(() -> new AppException("User not found"));

        return ResponseEntity.ok(user);
    }

    @PostMapping("/edit")
    public ResponseEntity<?> editUserProfile(@Valid @RequestBody User user) {
        userService.updateUser(user);
        userService.updateUserDetails(user.getDetails());
        return ResponseEntity.ok(user);
    }
}
