package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.mapper.ServiceMapper;
import com.netcracker.edu.backend.model.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Optional;

import static com.netcracker.edu.backend.utils.Constants.ServiceType;
import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
public class ServiceDaoImpl extends GenericDaoImpl<Service> implements ServiceDao {

    @Value("${db.query.service.insert}")
    private String sqlServiceInsert;

    @Value("${db.query.service.update}")
    private String sqlServiceUpdate;

    @Value("${db.query.service.getTypedById}")
    private String sqlGetTypedById;

    @Value("${db.query.service.getAllTyped}")
    private String sqlGetTyped;

    @Value("${db.query.service.getTypedOfStatus}")
    private String sqlGetTypedOfStatus;

    @Value("${db.query.service.getTypedByProviderId}")
    private String sqlGetTypedByProviderId;

    @Value("${db.query.service.getTypedOfStatusByProviderId}")
    private String sqlGetTypedOfStatusByProviderId;

    @Value("${db.query.service.getTypedByApproverId}")
    private String sqlGetTypedByApproverId;

    @Value("${db.query.service.getTypedOfStatusByApproverId}")
    private String sqlGetTypedOfStatusByApproverId;

    @Value("${db.query.service.countAllTyped}")
    private String sqlCountAllTyped;

    public ServiceDaoImpl() {
        super(new ServiceMapper(), TableName.SERVICE);
    }

    @Override
    protected String getInsertSql() {
        return sqlServiceInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Service entity) throws SQLException {

        int argNum = 1;
        statement.setLong(argNum++, entity.getTypeId());
        statement.setString(argNum++, entity.getName());

        Long approverId = entity.getApproverId();
        if (approverId == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setLong(argNum++, approverId);
        }

        statement.setLong(argNum++, entity.getProviderId());
        statement.setLong(argNum++, entity.getStatusId());
        statement.setLong(argNum++, entity.getLocationId());

        Long destinationId = entity.getDestinationId();
        if (destinationId == null) {
            statement.setNull(argNum++, Types.NULL);
        } else {
            statement.setLong(argNum++, destinationId);
        }

        statement.setLong(argNum++, entity.getNumberOfPeople());
        statement.setBigDecimal(argNum++, entity.getPrice());
        statement.setString(argNum++, entity.getDescription());
        statement.setString(argNum++, entity.getImgSrc());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlServiceUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Service entity) {
        return new Object[]{entity.getTypeId(), entity.getName(), entity.getApproverId(), entity.getProviderId(), entity.getStatusId(), entity.getLocationId(), entity.getDestinationId(), entity.getNumberOfPeople(), entity.getPrice(), entity.getDescription(), entity.getImgSrc()};
    }

    @Override
    public List<Service> getAllTrips() {
        return jdbcTemplate.query(sqlGetTyped, new Object[]{ServiceType.TRIP}, rowMapper);
    }

    @Override
    public Optional<Service> getTripById(long id) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetTypedById, new Object[]{ServiceType.TRIP, id}, rowMapper));
    }

    @Override
    public List<Service> getAllServices() {
        return jdbcTemplate.query(sqlGetTyped, new Object[]{ServiceType.SERVICE}, rowMapper);
    }

    @Override
    public Optional<Service> getServiceById(long id) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetTypedById, new Object[]{ServiceType.SERVICE, id}, rowMapper));
    }

    @Override
    public List<Service> getAllBundles() {
        return jdbcTemplate.query(sqlGetTyped, new Object[]{ServiceType.BUNDLE}, rowMapper);
    }

    @Override
    public Optional<Service> getBundleById(long id) {
        return Optional.of(jdbcTemplate.queryForObject(sqlGetTypedById, new Object[]{ServiceType.BUNDLE, id}, rowMapper));
    }

    @Override
    public List<Service> getAllTripsOfStatus(String status) {
        return jdbcTemplate.query(sqlGetTypedOfStatus, new Object[]{ServiceType.TRIP, status}, rowMapper);
    }

    @Override
    public List<Service> getTripsByProviderId(long id) {
        return jdbcTemplate.query(sqlGetTypedByProviderId, new Object[]{ServiceType.TRIP, id}, rowMapper);
    }

    @Override
    public List<Service> getTripsOfStatusByProviderId(String status, long id) {
        return jdbcTemplate.query(sqlGetTypedOfStatusByProviderId, new Object[]{ServiceType.TRIP, status, id}, rowMapper);
    }

    @Override
    public List<Service> getTripsByApproverId(long id) {
        return jdbcTemplate.query(sqlGetTypedByApproverId, new Object[]{ServiceType.TRIP, id}, rowMapper);
    }

    @Override
    public List<Service> getTripsOfStatusByApproverId(String status, long id) {
        return jdbcTemplate.query(sqlGetTypedOfStatusByApproverId, new Object[]{ServiceType.TRIP, status, id}, rowMapper);
    }


    @Override
    public List<Service> getAllServicesOfStatus(String status) {
        return jdbcTemplate.query(sqlGetTypedOfStatus, new Object[]{ServiceType.SERVICE, status}, rowMapper);
    }

    @Override
    public List<Service> getServicesByProviderId(long id) {
        return jdbcTemplate.query(sqlGetTypedByProviderId, new Object[]{ServiceType.SERVICE, id}, rowMapper);
    }

    @Override
    public List<Service> getServicesOfStatusByProviderId(String status, long id) {
        return jdbcTemplate.query(sqlGetTypedOfStatusByProviderId, new Object[]{ServiceType.SERVICE, status, id}, rowMapper);
    }

    @Override
    public List<Service> getAllBundlesOfStatus(String status) {
        return jdbcTemplate.query(sqlGetTypedOfStatus, new Object[]{ServiceType.BUNDLE, status}, rowMapper);
    }

    @Override
    public List<Service> getBundlesByProviderId(long id) {
        return jdbcTemplate.query(sqlGetTypedByProviderId, new Object[]{ServiceType.BUNDLE, id}, rowMapper);
    }

    @Override
    public List<Service> getBundlesOfStatusByProviderId(String status, long id) {
        return jdbcTemplate.query(sqlGetTypedOfStatusByProviderId, new Object[]{ServiceType.BUNDLE, status, id}, rowMapper);
    }

    @Override
    public long countAllTrips() {
        return jdbcTemplate.queryForObject(sqlCountAllTyped, new Object[]{ServiceType.TRIP}, long.class);
    }

    @Override
    public long countAllServices() {
        return jdbcTemplate.queryForObject(sqlCountAllTyped, new Object[]{ServiceType.SERVICE}, long.class);
    }

    @Override
    public long countAllBundles() {
        return jdbcTemplate.queryForObject(sqlCountAllTyped, new Object[]{ServiceType.BUNDLE}, long.class);
    }
}
