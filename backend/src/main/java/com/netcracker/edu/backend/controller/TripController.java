package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.netcracker.edu.backend.utils.Constants.ServiceType;

@RestController
@RequestMapping("/api/account/trips")
@CrossOrigin(":4200")
public class TripController {

    @Autowired
    private TripService tripService;

    @GetMapping()
    public ResponseEntity<?> loadAllTrips() {
        return ResponseEntity.ok(tripService.getAllTrips());
    }

    @PostMapping
    public ResponseEntity<?> createTrip(@Valid @RequestBody com.netcracker.edu.backend.model.Service service) {
        service.setType(ServiceType.TRIP); // dummy
        return ResponseEntity.ok(tripService.createTrip(service));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateTrip(@Valid @RequestBody com.netcracker.edu.backend.model.Service service) {
        tripService.updateTrip(service);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTrip(@PathVariable long id) {
        tripService.deleteTripById(id);
        return ResponseEntity.ok().build();
    }
    //To impl: specific methods etc
}
