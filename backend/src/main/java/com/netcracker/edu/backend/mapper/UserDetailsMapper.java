package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.UserDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.UserDetailsColumns;

public class UserDetailsMapper implements RowMapper<UserDetails> {

    @Override
    public UserDetails mapRow(ResultSet resultSet, int i) throws SQLException {
        UserDetails userDetails = new UserDetails();

        userDetails.setId(resultSet.getLong(UserDetailsColumns.USER_ID));
        userDetails.setFirstName(resultSet.getString(UserDetailsColumns.FIRST_NAME));
        userDetails.setLastName(resultSet.getString(UserDetailsColumns.LAST_NAME));
        userDetails.setRegistrationDate(resultSet.getTimestamp(UserDetailsColumns.REGISTRATION_DATE));
        userDetails.setLocationId(resultSet.getLong(UserDetailsColumns.LOCATION_ID));
        userDetails.setImageSrc(resultSet.getString(UserDetailsColumns.IMAGE_SRC));

        return userDetails;
    }
}