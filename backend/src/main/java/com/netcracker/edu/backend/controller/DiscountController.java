package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.model.Discount;
import com.netcracker.edu.backend.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/api/account/discounts")
@CrossOrigin(":4200")
public class DiscountController {

    @Autowired
    private DiscountService discountService;

    @GetMapping()
    public ResponseEntity<?> loadAllDiscounts() {
        return ResponseEntity.ok(discountService.getAllDiscounts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable long id) {
        return ResponseEntity.ok(discountService.getDiscountById(id));
    }

    @PostMapping
    public ResponseEntity<?> createDiscount(@Valid @RequestBody Discount discount) {
        return ResponseEntity.ok(discountService.createDiscount(discount));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateDiscount(@Valid @RequestBody Discount discount) {
        discountService.updateDiscount(discount);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDiscount(@PathVariable long id) {
        discountService.deleteDiscountById(id);
        return ResponseEntity.ok().build();
    }
    //To impl: specific methods etc
}
