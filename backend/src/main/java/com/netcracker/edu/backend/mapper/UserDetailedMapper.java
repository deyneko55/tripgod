package com.netcracker.edu.backend.mapper;

import com.netcracker.edu.backend.model.User;
import com.netcracker.edu.backend.model.UserDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.netcracker.edu.backend.utils.Constants.*;

public class UserDetailedMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();

        user.setId(resultSet.getLong(UserColumns.USER_ID));
        user.setAuthorityId(resultSet.getLong(UserColumns.AUTHORITY_ID));
        user.setAuthority(resultSet.getString(AuthorityColumns.NAME));
        user.setUsername(resultSet.getString(UserColumns.USERNAME));
        user.setEmail(resultSet.getString(UserColumns.EMAIL));
        user.setPassword(resultSet.getString(UserColumns.PASSWORD));
        user.setActive(resultSet.getBoolean(UserColumns.IS_ACTIVE));

        UserDetails userDetails = new UserDetails();

        userDetails.setId(user.getId());
        userDetails.setFirstName(resultSet.getString(UserDetailsColumns.FIRST_NAME));
        userDetails.setLastName(resultSet.getString(UserDetailsColumns.LAST_NAME));
        userDetails.setRegistrationDate(resultSet.getTimestamp(UserDetailsColumns.REGISTRATION_DATE));
        userDetails.setLocationId(resultSet.getLong(UserDetailsColumns.LOCATION_ID));
        userDetails.setImageSrc(resultSet.getString(UserDetailsColumns.IMAGE_SRC));

        user.setDetails(userDetails);

        return user;
    }
}
