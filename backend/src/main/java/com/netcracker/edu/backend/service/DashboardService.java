package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import com.netcracker.edu.backend.dao.UserDao;
import com.netcracker.edu.backend.dto.response.DashboardResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DashboardService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ServiceDao serviceDao;

    public DashboardResponse getDashboardResponse() {
        DashboardResponse response = new DashboardResponse();
        response.setUsersPerYear(getUsersPerYear());
        response.setServicesDistribution(getServicesDistribution());
        return response;
    }

    private Map<Integer, Long> getUsersPerYear() {

        return userDao.getAll()
                .stream()
                .parallel()
                .map(user -> user.getDetails())
                .map(userDetails -> userDetails.getRegistrationDate())
                .map(timestamp -> timestamp.toLocalDateTime())
                .map(localDateTime -> localDateTime.getYear())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    private Map<String, Long> getServicesDistribution() {

        Map<String, Long> servicesDistribution = new HashMap<>();

        servicesDistribution.put("Services", serviceDao.countAllServices());
        servicesDistribution.put("Trips", serviceDao.countAllTrips());
        servicesDistribution.put("Bundles", serviceDao.countAllBundles());

        return servicesDistribution;
    }
}
