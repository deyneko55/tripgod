package com.netcracker.edu.backend.utils;

import com.netcracker.edu.backend.dto.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public interface Constants {

    interface ResponseEntities {

        ResponseEntity BAD_REQ_USERNAME_TAKEN = new ResponseEntity<>(new ApiResponse(
                false, "Username is already taken!"
        ), HttpStatus.BAD_REQUEST);

        ResponseEntity BAD_REQ_EMAIL_TAKEN = new ResponseEntity<>(new ApiResponse(
                false, "Email Address already in use!"
        ), HttpStatus.BAD_REQUEST);

        ResponseEntity USER_REGISTERED_SUCCESSFULLY = new ResponseEntity<>(new ApiResponse(
                true, "User registered successfully"
        ), HttpStatus.CREATED);

        ResponseEntity APPROVER_REGISTERED_SUCCESSFULLY = new ResponseEntity<>(new ApiResponse(
                true, "Approver registered successfully"
        ), HttpStatus.CREATED);

        ResponseEntity PROVIDER_REGISTERED_SUCCESSFULLY = new ResponseEntity<>(new ApiResponse(
                true, "Provider registered successfully"
        ), HttpStatus.CREATED);
    }

    interface TableName {

        String USER = "\"USER\"";
        String USER_DETAILS = "user_details";
        String AUTHORITY = "authority";
        String SERVICE = "service";
        String SERVICE_TYPE = "service_type";
        String SERVICE_STATUS = "service_status";
        String DISCOUNT = "discount";
    }

    interface UserColumns {

        String USER_ID = "id";
        String AUTHORITY_ID = "authority_id";
        String USERNAME = "username";
        String EMAIL = "email";
        String PASSWORD = "password";
        String IS_ACTIVE = "is_active";
    }

    interface UserDetailsColumns {

        String USER_ID = "id";
        String FIRST_NAME = "first_name";
        String LAST_NAME = "last_name";
        String REGISTRATION_DATE = "registration_date";
        String LOCATION_ID = "location_id";
        String IMAGE_SRC = "image_src";
    }

    interface AuthorityColumns {

        String AUTHORITY_ID = "id";
        String NAME = "authority_name";
    }

    interface ServiceColumns {

        String SERVICE_ID = "id";
        String TYPE_ID = "type_id";
        String NAME = "service_name";
        String APPROVER_ID = "approver_id";
        String PROVIDER_ID = "provider_id";
        String STATUS_ID = "status_id";
        String LOCATION_ID = "location_id";
        String DESTINATION_ID = "destination_id";
        String NUMBER_OF_PEOPLE = "number_of_people";
        String PRICE = "price";
        String DESCRIPTION = "description";
        String IMAGE_SRC = "image_src";
    }

    interface DiscountColumns {

        String DISCOUNT_ID = "id";
        String SERVICE_ID = "service_id";
        String TYPE_ID = "type_id";
        String AMOUNT = "amount";
        String START_DATE = "start_date";
        String END_DATE = "end_date";
    }

    interface DiscountTypeColumns {

        String DISCOUNT_ID = "id";
        String TYPE_NAME = "discount_type_name";
    }

    interface RoleName {

        String ROLE_USER = "ROLE_USER";
        String ROLE_ADMIN = "ROLE_ADMIN";
        String ROLE_APPROVER = "ROLE_APPROVER";
        String ROLE_PROVIDER = "ROLE_PROVIDER";
    }

    interface ServiceType {

        String TRIP = "Trip";
        String SERVICE = "Service";
        String BUNDLE = "Bundle";
    }

    interface ServiceStatus {
        String DRAFT = "Draft";
        String OPEN = "Open";
        String ASSIGNED = "Assigned";
        String PUBLISHED = "Published";
        String UNDER_CLARIFICATION = "Under clarification";
        String REMOVED = "Removed";
        String ARCHIVED = "Archived";
    }


    interface DiscountType {
        String PERCENTAGE = "Percentage";
        String FIXED_DISCOUNT = "Fixed discount";
    }
}
