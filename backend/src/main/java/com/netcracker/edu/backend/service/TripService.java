package com.netcracker.edu.backend.service;

import com.netcracker.edu.backend.dao.ServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TripService {

    @Autowired
    private ServiceDao serviceDao;

    public Optional<com.netcracker.edu.backend.model.Service> getTripById(long id) {
        return serviceDao.getServiceById(id);
    }

    public com.netcracker.edu.backend.model.Service createTrip(com.netcracker.edu.backend.model.Service service) {
        return serviceDao.insert(service);
    }

    public void updateTrip(com.netcracker.edu.backend.model.Service service) {
        serviceDao.update(service);
    }

    public void deleteTripById(long id) {
        serviceDao.deleteById(id);
    }

    public void deleteTrip(com.netcracker.edu.backend.model.Service service) {
        serviceDao.delete(service);
    }

    public List<com.netcracker.edu.backend.model.Service> getAllTrips() {
        return serviceDao.getAllTrips();
    }

    public List<com.netcracker.edu.backend.model.Service> getAllTripsOfStatus(String status) {
        return serviceDao.getAllTripsOfStatus(status);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsByProviderId(long id) {
        return serviceDao.getTripsByProviderId(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsOfStatusByProviderId(String status, long id) {
        return serviceDao.getTripsOfStatusByProviderId(status, id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsByApproverId(long id) {
        return serviceDao.getTripsByApproverId(id);
    }

    public List<com.netcracker.edu.backend.model.Service> getTripsOfStatusByApproverId(String status, long id) {
        return serviceDao.getTripsOfStatusByApproverId(status, id);
    }

    public long countAllTrips() {
        return serviceDao.countAllTrips();
    }
}
