package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.GenericDao;
import com.netcracker.edu.backend.model.Identified;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
public abstract class GenericDaoImpl<T extends Identified> implements GenericDao<T> {

    private String tableName;

    @Value("${db.query.generic.getAll}")
    private String sqlGetAll;

    @Value("${db.query.generic.getById}")
    private String sqlGetUserById;

    @Value("${db.query.generic.deleteById}")
    private String sqlDeleteById;

    protected final RowMapper<T> rowMapper;
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    public GenericDaoImpl(RowMapper<T> rowMapper, String tableName) {
        this.rowMapper = rowMapper;
        this.tableName = tableName;
    }

    @PostConstruct
    private void initiateSql() {
        sqlGetAll = getSqlWithTableName(sqlGetAll);

        sqlGetUserById = getSqlWithTableName(sqlGetUserById);

        sqlDeleteById = getSqlWithTableName(sqlDeleteById);
    }

    /**
     * Here you should return your table-specified insert query
     *
     * @return Insert query
     */
    protected abstract String getInsertSql();

    /**
     * Fill insert statement with data in order that matches your insert query
     *
     * @return Filled prepared statement
     */
    protected abstract PreparedStatement prepareStatementForInsert(PreparedStatement statement, T entity) throws SQLException;

    /**
     * Here you should return your table-specified update query
     *
     * @return Update query
     */
    protected abstract String getUpdateSql();

    /**
     * Here you should update arguments in order that matches your update query
     *
     * @return Update arguments
     */
    protected abstract Object[] getArgsForUpdate(T entity);

    private String getSqlWithTableName(String sql) {
        return String.format(sql, tableName);
    }

    private Object[] getArgsWithAppendedId(Object[] args, long id) {

        List<Object> objects = new ArrayList<>(Arrays.asList(args));
        objects.add(id);

        return objects.toArray();
    }

    protected String appendSimpleWhereClause(String baseSql, String columnName, Object columnValue) {

        return baseSql + " WHERE " + columnName + " = " + columnValue.toString();
    }

    @Override
    public List<T> getAll() {
        log.debug("Getting all elements in {}: ", this.getClass().getName());

        return jdbcTemplate.query(sqlGetAll, rowMapper);
    }

    @Override
    public List<T> getAllWithSimpleCheck(String columnName, Object columnValue) {
        log.debug("Getting all elements where {} = {} in {}: ", columnName, columnValue.toString(), this.getClass().getName());

        return jdbcTemplate.query(appendSimpleWhereClause(sqlGetAll, columnName, columnValue), rowMapper);
    }

    @Override
    public Optional<T> getById(Long id) {
        log.debug("Getting element by id = {} in {}: ", id, this.getClass().getName());

        return Optional.of(jdbcTemplate.queryForObject(sqlGetUserById, new Object[]{id}, rowMapper));
    }

    @Override
    public T insert(T entity) {
        log.debug("Inserting element {} in {}: ", entity.toString(), this.getClass().getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        this.jdbcTemplate.update(new PreparedStatementCreator() {

            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement statement = connection.prepareStatement(getInsertSql(), new String[]{"id"});
                prepareStatementForInsert(statement, entity);
                return statement;
            }

        }, keyHolder);

        entity.setId(keyHolder.getKey().longValue());

        return entity;
    }

    @Override
    public void update(T entity) {
        log.debug("Updating element {} in {}: ", entity.toString(), this.getClass().getName());

        jdbcTemplate.update(getUpdateSql(), getArgsWithAppendedId(getArgsForUpdate(entity), entity.getId()));
    }

    @Override
    public void delete(T entity) {
        log.debug("Deleting element {} in {} : ", entity.toString(), this.getClass().getName());

        jdbcTemplate.update(sqlDeleteById, entity.getId());
    }

    @Override
    public void deleteById(Long id) {
        log.debug("Deleting element by id = {} in {}: ", id, this.getClass().getName());

        jdbcTemplate.update(sqlDeleteById, id);
    }
}
