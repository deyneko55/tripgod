package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Service;

import java.util.List;
import java.util.Optional;

public interface ServiceDao extends GenericDao<Service> {

    List<Service> getAllTrips();

    Optional<Service> getTripById(long id);

    List<Service> getAllTripsOfStatus(String status);

    List<Service> getTripsByProviderId(long id);

    List<Service> getTripsOfStatusByProviderId(String status, long id);

    List<Service> getTripsByApproverId(long id);

    List<Service> getTripsOfStatusByApproverId(String status, long id);

    List<Service> getAllServices();

    Optional<Service> getServiceById(long id);

    List<Service> getAllServicesOfStatus(String status);

    List<Service> getServicesByProviderId(long id);

    List<Service> getServicesOfStatusByProviderId(String status, long id);

    List<Service> getAllBundles();

    Optional<Service> getBundleById(long id);

    List<Service> getAllBundlesOfStatus(String status);

    List<Service> getBundlesByProviderId(long id);

    List<Service> getBundlesOfStatusByProviderId(String status, long id);

    long countAllTrips();

    long countAllServices();

    long countAllBundles();

}
