package com.netcracker.edu.backend.controller;

import com.netcracker.edu.backend.dto.request.LoginRequest;
import com.netcracker.edu.backend.dto.request.SignUpRequest;
import com.netcracker.edu.backend.dto.response.JwtAuthenticationResponse;
import com.netcracker.edu.backend.security.JwtTokenProvider;
import com.netcracker.edu.backend.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(":4200")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private AuthService authService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(new JwtAuthenticationResponse(authService.authenticateUser(
                loginRequest, authenticationManager, tokenProvider)));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        return authService.registerUser(signUpRequest, passwordEncoder);
    }
}

