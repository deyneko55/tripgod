package com.netcracker.edu.backend.dao.impl;

import com.netcracker.edu.backend.dao.DiscountDao;
import com.netcracker.edu.backend.mapper.DiscountDetailedMapper;
import com.netcracker.edu.backend.mapper.DiscountMapper;
import com.netcracker.edu.backend.model.Discount;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import static com.netcracker.edu.backend.utils.Constants.TableName;

@Repository
public class DiscountDaoImpl extends GenericDaoImpl<Discount> implements DiscountDao {

    private static final RowMapper<Discount> rowMapperDetailed = new DiscountDetailedMapper();
    @Value("${db.query.discount.insert}")
    private String sqlDiscountInsert;
    @Value("${db.query.discount.update}")
    private String sqlDiscountUpdate;
    @Value("${db.query.discount.getDiscountsOfType}")
    private String sqlGetDiscountsOfType;
    @Value("${db.query.discount.getDiscountsOfTypeDetailed}")
    private String sqlGetDiscountsOfTypeDetailed;
    @Value("${db.query.discount.getDiscountsByServiceId}")
    private String sqlGetDiscountsByServiceId;
    @Value("${db.query.discount.getDiscountsByServiceIdDetailed}")
    private String sqlGetDiscountsByServiceIdDetailed;
    @Value("${db.query.discount.getDiscountsForPeriod}")
    private String sqlGetDiscountsForPeriod;
    @Value("${db.query.discount.getDiscountsForPeriodDetailed}")
    private String sqlGetDiscountsForPeriodDetailed;

    public DiscountDaoImpl() {
        super(new DiscountMapper(), TableName.DISCOUNT);
    }

    @Override
    protected String getInsertSql() {
        return sqlDiscountInsert;
    }

    @Override
    protected PreparedStatement prepareStatementForInsert(PreparedStatement statement, Discount entity) throws SQLException {

        int argNum = 1;
        statement.setLong(argNum++, entity.getServiceId());
        statement.setLong(argNum++, entity.getTypeId());
        statement.setDouble(argNum++, entity.getAmount());
        statement.setTimestamp(argNum++, entity.getStartDate());
        statement.setTimestamp(argNum++, entity.getEndDate());

        return statement;
    }

    @Override
    protected String getUpdateSql() {
        return sqlDiscountUpdate;
    }

    @Override
    protected Object[] getArgsForUpdate(Discount entity) {
        return new Object[]{entity.getServiceId(), entity.getTypeId(), entity.getAmount(), entity.getStartDate(), entity.getEndDate()};
    }

    @Override
    public List<Discount> getDiscountsOfType(String type) {
        return jdbcTemplate.query(sqlGetDiscountsOfType, new Object[]{type}, rowMapper);
    }

    @Override
    public List<Discount> getDiscountsByServiceId(long serviceId) {
        return jdbcTemplate.query(sqlGetDiscountsByServiceId, new Object[]{serviceId}, rowMapper);
    }

    @Override
    public List<Discount> getDiscountsForPeriod(Timestamp from, Timestamp to) {
        return jdbcTemplate.query(sqlGetDiscountsForPeriod, new Object[]{from, to}, rowMapper);
    }

    @Override
    public List<Discount> getDiscountsOfTypeDetailed(String type) {
        return jdbcTemplate.query(sqlGetDiscountsOfTypeDetailed, new Object[]{type}, rowMapperDetailed);
    }

    @Override
    public List<Discount> getDiscountsByServiceIdDetailed(long serviceId) {
        return jdbcTemplate.query(sqlGetDiscountsByServiceIdDetailed, new Object[]{serviceId}, rowMapperDetailed);
    }

    @Override
    public List<Discount> getDiscountsForPeriodDetailed(Timestamp from, Timestamp to) {
        return jdbcTemplate.query(sqlGetDiscountsForPeriodDetailed, new Object[]{from, to}, rowMapperDetailed);
    }
}
