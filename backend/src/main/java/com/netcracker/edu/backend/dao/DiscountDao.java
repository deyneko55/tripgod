package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.Discount;

import java.sql.Timestamp;
import java.util.List;

public interface DiscountDao extends GenericDao<Discount> {

    List<Discount> getDiscountsOfType(String type);

    List<Discount> getDiscountsOfTypeDetailed(String type);

    List<Discount> getDiscountsByServiceId(long serviceId);

    List<Discount> getDiscountsByServiceIdDetailed(long serviceId);

    List<Discount> getDiscountsForPeriod(Timestamp from, Timestamp to);

    List<Discount> getDiscountsForPeriodDetailed(Timestamp from, Timestamp to);

}
