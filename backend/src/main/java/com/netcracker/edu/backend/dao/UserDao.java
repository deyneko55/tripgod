package com.netcracker.edu.backend.dao;

import com.netcracker.edu.backend.model.User;

import java.util.Optional;

public interface UserDao extends GenericDao<User> {

    Optional<User> getDetailedUserById(long id);

    Optional<User> getDetailedUserByUsername(String username);

    Optional<User> getDetailedUserByEmail(String email);

    Optional<User> getDetailedUserByUsernameOrEmail(String usernameOrEmail);

    Optional<User> getUserByUsernameOrEmail(String usernameOrEmail);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);
}
