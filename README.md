Short guide/help:

To run project u must have Angular CLI (+ npm package, node.js) in addition to maven, jdk etc.

To run application:
1. Make sure that .../resources in backend include:
    - "static" folder and "templates" folder.
2. mvn install on frontend module.
3. mvn clean install (in directory root).
    *parent project must be built
    *Idea UI may be used too
    
4. Run spring boot application.
    *npm scripts are implemented by Idea
    *use mvn spring-boot:run in .../backend directory
    
5. Go to http://localhost:8080.